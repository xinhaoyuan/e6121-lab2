#include "link.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IRReader/IRReader.h"
#include <memory>

using namespace llvm;
using namespace bounds;

bool LinkWithRT::runOnModule(Module &M) {
  Linker linker(M);
  SMDiagnostic err;
  std::unique_ptr<Module> rt = getLazyIRFileModule(pathToRT, err, M.getContext(), false); 
  if (linker.linkInModule(std::move(rt)))
    assert(0 && "linking in library failed!");
  return true;
}

char LinkWithRT::ID = 0;

namespace {
static RegisterPass<LinkWithRT>
X("link-bounds-rt", "Linking with Bounds Checking Runtime",
  false /* Only looks at CFG */,
  false /* Analysis Pass */);
}
