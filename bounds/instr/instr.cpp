#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "instr.h"

using namespace bounds;

void BoundsInstr::getAnalysisUsage(AnalysisUsage &AU) const
{
}

bool BoundsInstr::doInitialization(Module &M)
{
  TD = new DataLayout(&M);
  // Lab 2 TODO
  //
  // Acquire pointers to bounds-checking runtime methods.
  // How to add an external declaration for the function:
  //
  // getOrInsertFunction
  // http://llvm.org/docs/ProgrammersManual.html#the-module-class.
  //
  // How to create a type: 
  // http://llvm.org/docs/ProgrammersManual.html#how-to-create-types.
  //
  // Note: the page is slightly out dated.  You need to pass an
  // LLVMContext object to the get() method of TypeBuilder, which you can
  // get by calling M.getContext()
  AllocVarF = NULL;
  FreeVarF = NULL;
  CheckDerefF = NULL;
  CheckGEPF = NULL;
  AllocArgsF = NULL;

  // Lab 2 TODO
  //
  // Replace unsafe library functions with our safe versions which check bounds
  const char *libfuncs[] = {
    "malloc", // ==> Malloc
    "free",   // ==> Free
    "strcpy", // ==> Strcpy
    "strcat", // ==> Strcat
  };
  for(unsigned i=0; i<sizeof(libfuncs)/sizeof(libfuncs[0]); ++i) {
    // ...
  }
  return true;
}

bool BoundsInstr::runOnFunction(Function &F)
{
  BuilderTy TheBuilder(F.getContext(), TargetFolder(*TD));
  builder = &TheBuilder;
  trapBB = NULL;

  std::vector<Instruction*> allocas, returns, mems, geps;
  for (inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i) {
    Instruction *I = &*i;
    // Add alloca instructions to work list
    if(isa<AllocaInst>(I)) allocas.push_back(I);

    // Collect return instructions, used when instrumenting alloca
    if(isa<ReturnInst>(I)) returns.push_back(I);

    // Add instructions that access memory to work list;
    // see HANDLE_MEMORY_INST in include/llvm/Instruction.def
    if(isa<LoadInst>(I) || isa<StoreInst>(I) 
      || isa<AtomicCmpXchgInst>(I) || isa<AtomicRMWInst>(I))
      mems.push_back(I);

    // Add GetElementPtrInst (GEP) instructions to work list.  GEP does
    // pointer arithmetic.
    // http://llvm.org/docs/GetElementPtr.html
    if(isa<GetElementPtrInst>(I))
      geps.push_back(I);
  }

  bool changed = false;
  // Instrument alloca instructions
  for (std::vector<Instruction*>::iterator i = allocas.begin(),
         e = allocas.end(); i != e; ++i)
    changed |= instrAlloca(*i, returns);

  // Instrument memory access instructions
  for (std::vector<Instruction*>::iterator i = mems.begin(),
         e = mems.end(); i != e; ++i)
    changed |= instrMem(*i);

  // Instrument GEP instructions
  for (std::vector<Instruction*>::iterator i = geps.begin(),
         e = geps.end(); i != e; ++i)
    changed |= instrGEP(*i);

  if(F.getName() == "main") {
    // Instrument global variables
    Module *M = F.getParent();
    for (Module::global_iterator i = M->global_begin(), e = M->global_end();
         i != e; ++i)
      changed |= instrGlobal(&*i, F);

    // Instrument arguments to main
    if(F.arg_size())
      changed |= instrArgs(F);
  }

  return changed;
}

bool BoundsInstr::instrAlloca(Instruction *I, std::vector<Instruction*>& returns)
{
  // Lab 2 TODO
  //
  // Add "AllocVar(ptr, size)" after an alloca instruction
  // 
  // IRBuilder is, unsurprisingly, quite handy at building IR instructions.
  // http://llvm.org/releases/2.6/docs/tutorial/JITTutorial2.html

  // ...

  // Lab 2 TODO
  //
  // Add "FreeVar(ptr)" before each return instruction

  // ...

  return true;
}

bool BoundsInstr::instrMem(Instruction *I)
{
  Value *ptr, *val;

  if (LoadInst *LI = dyn_cast<LoadInst>(I)) {
    ptr = LI->getPointerOperand();
    val = LI;
  } else if (StoreInst *SI = dyn_cast<StoreInst>(I)) {
    ptr = SI->getPointerOperand();
    val = SI->getValueOperand();
  } else if (AtomicCmpXchgInst *AI = dyn_cast<AtomicCmpXchgInst>(I)) {
    ptr = AI->getPointerOperand();
    val = AI->getCompareOperand();
  } else if (AtomicRMWInst *AI = dyn_cast<AtomicRMWInst>(I)) {
    ptr = AI->getPointerOperand();
    val = AI->getValOperand();
  } else 
    llvm_unreachable("unknown Instruction type");

  // Lab 2 TODO
  //
  // Add "CheckDeref(ptr, size)" before each memory access instruction

  // ...

  return true;
}

bool BoundsInstr::instrGlobal(GlobalVariable *GV, Function &F)
{
  // Lab 2 TODO
  //
  // Add "AllocVar(ptr, size)" at entry of main() for each global variable

  // ...

  return true;
}

bool BoundsInstr::instrGEP(Instruction *I)
{
  // Lab 2 TODO
  //
  // Add "CheckGEP(oldp, newp)" after each GEP instruction

  // ...

  return true;
}

bool BoundsInstr::instrArgs(Function &F)
{
  // Lab 2 TODO
  //
  // Add "AllocArgs(argc, argv)" at entry of main()

  // ...

  return true;
}

char BoundsInstr::ID = 0;

namespace {
static RegisterPass<BoundsInstr>
X("bounds-instr", "Bounds Checking Instrumentation",
  false /* Only looks at CFG */,
  false /* Analysis Pass */);
}
