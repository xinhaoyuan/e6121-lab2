/*  -*- Mode: C++ -*- */
#ifndef __BOUNDS_INSTR_H
#define __BOUNDS_INSTR_H

#include <vector>
#include "llvm/Pass.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/TargetFolder.h"

using namespace llvm;

namespace bounds {

struct BoundsInstr: public FunctionPass {
  static char ID;

  BoundsInstr(): FunctionPass(ID) {}
  virtual void getAnalysisUsage(AnalysisUsage &AU) const;
  virtual bool doInitialization(Module &M);
  virtual bool runOnFunction(Function &F);

protected:
  bool instrAlloca(Instruction *I, std::vector<Instruction*>& returns);
  bool instrMem(Instruction *I);
  bool instrGlobal(GlobalVariable *GV, Function &F);
  bool instrGEP(Instruction *I);
  bool instrArgs(Function &F);

  typedef IRBuilder<TargetFolder, IRBuilderDefaultInserter> BuilderTy;
  DataLayout *TD;
  BuilderTy *builder;
  BasicBlock *trapBB;
  Value *AllocVarF, *FreeVarF, *CheckDerefF, *CheckGEPF, *AllocArgsF;
};

}

#endif
