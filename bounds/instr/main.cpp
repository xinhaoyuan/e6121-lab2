#include "llvm/Linker/Linker.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/IR/LegacyPassNameParser.h"

#include "instr.h"
#include "link.h"

using namespace std;
using namespace llvm;
using namespace bounds;

static cl::list<const PassInfo*, bool, PassNameParser>
PassList(cl::desc("Passes available:"));

static cl::opt<std::string>
InputFilename(cl::Positional, cl::desc("<input bitcode file>"),
    cl::init("-"), cl::value_desc("filename"));

static cl::opt<bool>
OutputAssembly("S", cl::desc("Write output as LLVM assembly"));

static bool endsWith(const string& str, const char* suffix) {
  size_t len = str.length();
  size_t suffix_len = strlen(suffix);
  if(len < suffix_len)
    return false;
  return str.compare(len-suffix_len, suffix_len, suffix) == 0;
}

int main(int argc, char **argv) {
  std::string ProgName = argv[0];
  cl::ParseCommandLineOptions(argc, argv,
    "augment input with bounds checking for stack variables\n");

  // Load input bitcode file
  LLVMContext Context;
  SMDiagnostic Err;
  std::unique_ptr<Module> M = parseIRFile(InputFilename, Err, Context);
  if (M.get() == 0) {
    Err.print(argv[0], errs());
    return 1;
  }

  // Compute output filename
  string OutputFilename = InputFilename;
  if(endsWith(OutputFilename, ".ll") || endsWith(OutputFilename, ".bc"))
    OutputFilename = OutputFilename.substr(0, OutputFilename.length()-3);
  string ext = (OutputAssembly? ".ll" : ".bc");
  OutputFilename   = OutputFilename + "-hardened" + ext;
  error_code ErrorInfo;
  raw_ostream *Out = new raw_fd_ostream(OutputFilename.c_str(),
                                        ErrorInfo, sys::fs::OpenFlags::F_None);
  assert(Out   && "can't open output file!");

  // Compute path to bounds-checking runtime lib/rt.bc
  std::string PathToRT = ProgName;
  size_t pos = PathToRT.rfind('/');
  if(pos == string::npos)
    pos = 0;
  PathToRT.replace(pos, PathToRT.length()-pos, "/../runtime/rt.bc");

  legacy::PassManager Passes;
  // Pass to add bounds-checking instrumentation
  Passes.add(new BoundsInstr);
  // Pass to link bounds-checking runtime
  Passes.add(new LinkWithRT(PathToRT));
  Passes.add(createVerifierPass());
  if (OutputAssembly)
    Passes.add(createPrintModulePass(*Out));
  else
    Passes.add(createBitcodeWriterPass(*Out));
  Passes.run(*M.get());

  delete Out;
  return 0;
}
