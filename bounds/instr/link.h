/*  -*- Mode: C++ -*- */
#ifndef __BOUNDS_LINK_H
#define __BOUNDS_LINK_H

#include <string>
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/Path.h"

namespace bounds {

/// Pass to link a module with the bounds-checking runtime
///
struct LinkWithRT: public llvm::ModulePass {
  LinkWithRT(): ModulePass(ID) {}
  LinkWithRT(const std::string& rt): ModulePass(ID), pathToRT(rt) {}

  virtual bool runOnModule(llvm::Module &M);

  /// path to the bounds-checking runtime
  std::string pathToRT;
  static char ID;
};

}

#endif
