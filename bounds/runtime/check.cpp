#include <set>
#include <err.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "check.h"

using namespace std;

/// Records the bounds info for a variable at address @v with size @size
///
void AllocVar(void *v, size_t size)
{
  // Lab 2 TODO

  // ...

}

/// Removes the bounds information for the variable at address @v
///
void FreeVar(void *v)
{
  // Lab 2 TODO

  // ...

}

/// Checks that the dereference of [p, p+size) is not off bounds
///
void CheckDeref(void *p, size_t size)
{
  // Lab 2 TODO

  // ...

}

/// Checks that pointer arithmetic @newp = @oldp + ... does not create an
/// off-bound pointer
///
void CheckGEP(void *oldp, void *newp)
{
  // Lab 2 TODO

  // ...

}

/// Records bounds info for the argument @argv
///
void AllocArgs(int argc, char *argv[])
{
  AllocVar((void*)argv, argc * sizeof(argv[0]));
  for(int i=0; i<argc; ++i)
    AllocVar(argv[i], strlen(argv[i])+1);
}

/// Calls malloc and records the bounds for the allocated memory
///
void* Malloc(size_t size)
{
  void *p = malloc(size);
  if(p)
    AllocVar(p, size);
  return p;
}

/// Checks that the free is valid before freeing memory
///
void Free(void *p)
{
  // Lab 2 TODO

  // ... check for errors

  FreeVar(p);
  free(p);
}

/// strcat with bounds checking
///
char *Strcat(char *dest, const char *src)
{
  size_t len = strlen(dest) + strlen(src) + 1;
  CheckDeref(dest, len);
  return strcat(dest, src);
}

/// strcpy with bounds checking
///
char *Strcpy(char *dest, const char *src)
{
  CheckDeref(dest, strlen(src)+1);
  return strcpy(dest, src);
}
