/*  -*- Mode: C++ -*- */
#ifndef __BOUNDS_CHECK_H
#define __BOUNDS_CHECK_H

// disable C++ name mangling for functions in the runtime
#ifdef __cplusplus
extern "C" {
#endif
  // basic runtime methods
  void AllocVar(void *v, size_t size);
  void FreeVar(void *v);
  void CheckDeref(void *p, size_t size);
  void CheckGEP(void *oldp, void *newp);
  void AllocArgs(int argc, char *argv[]);

  // safe versions of common library functions
  void* Malloc(size_t size);
  void Free(void *p);
  char *Strcat(char *dest, const char *src);
  char *Strcpy(char *dest, const char *src);

#ifdef __cplusplus
}
#endif

#endif
